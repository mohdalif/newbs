package com.infotrek.model;

import java.util.List;

public class PrimeFactorModel {
  
  private Object number;
  private List<Long> decomposition;
  private String error;

  public Object getFactorNumber() {
    return number;
  }

  public void setFactorNumber(Object factorNumber) {
    this.number = factorNumber;
  }

  public List<Long> getDecompose() {
    return decomposition;
  }

  public void setDecompose(List<Long> decompose) {
    this.decomposition = decompose;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }
  
  

}
