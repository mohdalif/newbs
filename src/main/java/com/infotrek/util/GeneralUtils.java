package com.infotrek.util;

import java.util.ArrayList;
import java.util.List;
import com.infotrek.model.PrimeFactorModel;

public class GeneralUtils {

  public static List<Long> getPrimeFactors(long n) {
    List<Long> decompose = new ArrayList<>();
    // for each potential factor
    for (long factor = 2; factor * factor <= n; factor++) {
      // if factor is a factor of n, repeatedly divide it out
      while (n % factor == 0) {
        decompose.add(factor);
        n = n / factor;
      }
    }
    // if biggest factor occurs only once, n > 1
    if (n > 1) {
      // System.out.println(n);
      decompose.add(n);
    } else {
      // System.out.println();
    }
    return decompose;
  }

  public static PrimeFactorModel getPrimeFactor(String qParam) {
    int exp = 6;
    PrimeFactorModel pfModel = new PrimeFactorModel();
    // String qParam = req.queryParams("number");
    boolean isNumeric = qParam.chars().allMatch(Character::isDigit);
    if (isNumeric) {
      long n = Long.parseLong(qParam);
      if (n > (1 * Math.pow(10, exp))) {
        pfModel.setFactorNumber(n);
        pfModel.setError("too big number (>1e" + exp + ")");
      } else {
        List<Long> decompose = GeneralUtils.getPrimeFactors(n);
        pfModel.setFactorNumber(n);
        pfModel.setDecompose(decompose);
      }
    } else {
      pfModel.setFactorNumber(qParam);
      pfModel.setError("not a number");
    }
    return pfModel;
  }

  public static int getHerokuAssignedPort() {
    ProcessBuilder processBuilder = new ProcessBuilder();
    if (processBuilder.environment().get("PORT") != null) {
      return Integer.parseInt(processBuilder.environment().get("PORT"));
    }
    return 4567; // return default port if heroku-port isn't set (i.e. on localhost)
  }

}


