package com.infotrek;

import static spark.Spark.get;
import static spark.Spark.port;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.infotrek.model.JsonModel;
import com.infotrek.model.PrimeFactorModel;
import com.infotrek.util.GeneralUtils;
import com.infotrek.util.JsonTransformer;

import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;

public class App {

  public static void main(String[] args) {
    

    port(GeneralUtils.getHerokuAssignedPort());
    

    get("/", (request, response) -> {
      Map<String, Object> attributes = new HashMap<>();
      attributes.put("message", "Hello Yose");
      return new ModelAndView(attributes, "index.ftl");
    }, new FreeMarkerEngine());
    

    get("/astroport", (request, response) -> {
      Map<String, Object> attributes = new HashMap<>();
      attributes.put("message", "Hello Yose");
      return new ModelAndView(attributes, "astrosport.ftl");

    }, new FreeMarkerEngine());
    

    get("/minesweeper", (request, response) -> {
      Map<String, Object> attributes = new HashMap<>();
      attributes.put("message", "Hello Yose");
      // src/main/resources/spark/template/freemarker
      return new ModelAndView(attributes, "minesweeper.ftl");
    }, new FreeMarkerEngine());
    

    get("/ping", (req, resp) -> {
      resp.type("application/json");
      JsonModel ping = new JsonModel();
      ping.setAlive(true);
      return ping;

    }, new JsonTransformer());
    

    get("/primeFactors/ui", (request, response) -> {
      Map<String, Object> attributes = new HashMap<>();
      if(request.queryParams().size() > 0) {
      String inputNum = request.queryParams("number");
      attributes.put("results", GeneralUtils.getPrimeFactor(inputNum));
      }
      return new ModelAndView(attributes, "primefactor.ftl");
    }, new FreeMarkerEngine());
    

    get("/primeFactors", (req, resp) -> {
      resp.type("application/json");
      String[] qParams = req.queryParamsValues("number");
      if (qParams.length > 1) {
        List<PrimeFactorModel> pfModels = new ArrayList<>();
        for (int i = 0; i < qParams.length; i++) {
          pfModels.add(GeneralUtils.getPrimeFactor(qParams[i]));
        }
        return pfModels;
      } else {
        return GeneralUtils.getPrimeFactor(qParams[0]);
      }
    }, new JsonTransformer());


    
  }

}
