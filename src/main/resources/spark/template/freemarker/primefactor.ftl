<h2 id="title">Prime Factor</h2>
<p id="invitation">
Update your server to accept /primeFactors/ui request and serve a page with a form to let a user ask for the decomposition of a number.
</p>
<form method="get">
<input id="number" name="number" type="text" />
<button id="go" type="submit" value="Go">GO</button>
</form>
<#if results??>
	<#assign resultsinput = results.getDecompose() />
	<#assign x = 1 />
	<span>Result: </span>
	<span id="result">${results.getFactorNumber()} = <#list resultsinput as factor>${factor} x </#list></span>
</#if>