Hello Yose

<p></p>


<a id="repository-link" href="https://bitbucket.org/mohdalif/newbs/src">readme</a>


<h2>Contact Team Newbs</h2>
<p>We would love to hear from you!</p>

<h3>Email Address:</h3>
<p>teamnewbs@gmail.com</p>
<a id="contact-me-link" href="http://facebook.com/teamnewbs">Team Newbs Facebook Page</a>

<h3>Team Newbs Members:</h3>
<ul>
	<li>Alif</li>
	<li>Chee Keong</li>
	<li>Chin Thung</li>
	<li>Kavitha</li>
	<li>Kelvin</li>
</ul>

<h3>Links:</h3>
<a id="ping-challenge-link" href="https://newbs.herokuapp.com/ping">Ping Challenge</a>
<a id="astroport-link" href="/astroport">Astroport</a>

